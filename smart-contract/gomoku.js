/* Tic tac toe smart contract

MIT License

Copyright (c) 2018 Sergei Glushchenko

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE. */

/* global LocalContractStorage */
/* eslint no-underscore-dangle: ["error",
          { "allowAfterThis": true, "enforceInMethodNames": false }] */

class Player {
  constructor(nickName, address, pubkey) {
    this.nickName = nickName;
    this.address = address;
    this.pubkey = pubkey;
    this.games = 0;
    this.wins = 0;
    this.draws = 0;
    this.elo = 1600;
  }

  toString() {
    return JSON.stringify({
      nickName: this.nickName,
      address: this.address,
      pubkey: this.pubkey,
      games: this.games,
      wins: this.wins,
      draws: this.draws,
      elo: this.elo,
    });
  }

  static fromString(s) {
    const o = JSON.parse(s);
    const p = new Player(o.nickName, o.address, o.pubkey);
    p.games = o.games;
    p.wins = o.wins;
    p.draws = o.draws;
    p.elo = o.elo;
    return p;
  }

  verify(msg, signature) {
    return true;
  }
}

const BOARD_ROWS = 15;
const BOARD_COLS = 15;

class Game {
  constructor(id, players) {
    this.id = id;
    this.players = players;
    this.playerAccs = [];
    this.moves = [];
    this.currentMove = 0;
    this.board = new Array((BOARD_ROWS * BOARD_COLS) + 1).join(' ');
    this.lastSig = '';
    this.final = false;
    this.winner = null;
  }

  toString() {
    return JSON.stringify({
      id: this.id,
      players: this.players,
      moves: this.moves,
      final: this.final,
      winner: this.winner,
    });
  }

  static fromString(s) {
    const o = JSON.parse(s);
    const game = new Game(o.id, o.players);
    game.moves = o.moves;
    game.final = o.final;
    game.winner = o.winner;
    return game;
  }


  isWin() {
    const rDiagonal = (r, c) => {
      let w = 0;
      const sq = this.board[(r * BOARD_COLS) + c];
      for (let x = c, y = r; x < BOARD_COLS && y < BOARD_ROWS; x += 1, y += 1) {
        if (sq === this.board[(y * BOARD_COLS) + x]) {
          w += 1;
        } else {
          break;
        }
      }
      return (w >= 5);
    };

    const lDiagonal = (r, c) => {
      let w = 0;
      const sq = this.board[(r * BOARD_COLS) + c];
      for (let x = c, y = r; x >= 0 && y < BOARD_ROWS; x -= 1, y += 1) {
        if (sq === this.board[(y * BOARD_COLS) + x]) {
          w += 1;
        } else {
          break;
        }
      }
      return (w >= 5);
    };

    const rightward = (r, c) => {
      let w = 0;
      const sq = this.board[(r * BOARD_COLS) + c];
      for (let x = c; x < BOARD_COLS; x += 1) {
        if (sq === this.board[(r * BOARD_COLS) + x]) {
          w += 1;
        } else {
          break;
        }
      }
      return (w >= 5);
    };

    const downward = (r, c) => {
      let w = 0;
      const sq = this.board[(r * BOARD_COLS) + c];
      for (let y = r; y < BOARD_ROWS; y += 1) {
        if (sq === this.board[(y * BOARD_COLS) + c]) {
          w += 1;
        } else {
          break;
        }
      }
      return (w >= 5);
    };

    for (let i = 0; i < BOARD_ROWS; i += 1) {
      for (let j = 0; j < BOARD_COLS; j += 1) {
        if (this.board[(i * BOARD_COLS) + j] !== ' ') {
          if (downward(i, j) ||
              rightward(i, j) ||
              lDiagonal(i, j) ||
              rDiagonal(i, j)) {
            return this.board[(i * BOARD_COLS) + j];
          }
        }
      }
    }

    return false;
  }

  isDraw() {
    for (let i = 0; i < BOARD_ROWS * BOARD_COLS; i += 1) {
      if (this.board[i] === ' ') {
        return false;
      }
    }
    return true;
  }

  isValidMove(row, col) {
    if (this.board[(row * BOARD_COLS) + col] !== ' ') {
      return false;
    }
    return true;
  }

  applyMove(move) {
    if (!this.verifyMove(move)) {
      return false;
    }

    const [, , address, , row, col, sig] = move;
    const mark = (this.playerAccs[0].address === address) ? 'X' : 'O';
    const index = (row * BOARD_COLS) + col;

    this.board = this.board.substr(0, index) + mark
                 + this.board.substr(index + mark.length);
    this.currentMove += 1;
    this.lastSig = sig;
    this.moves.push(move);

    return true;
  }

  applyMoveNoVerify(move) {
    const [, , address, , row, col, sig] = move;
    const mark = (this.playerAccs[0].address === address) ? 'X' : 'O';
    const index = (row * BOARD_COLS) + col;

    this.board = this.board.substr(0, index) + mark
                 + this.board.substr(index + mark.length);
    this.currentMove += 1;
    this.lastSig = sig;

    return true;
  }

  verifyMove(move) {
    const player = this.playerAccs[this.currentMove % 2];

    const [id, tag, address, moveN, row, col, sig] = move;

    if (id !== this.id) {
      return false;
    }

    if (tag !== 'move') {
      return false;
    }

    if (address !== player.address) {
      return false;
    }

    if (moveN !== this.currentMove) {
      return false;
    }

    const moveString = JSON.stringify([id, 'move', address,
      moveN, row, col, this.lastSig]);

    if (!player.verify(moveString, sig)) {
      return false;
    }

    return this.isValidMove(row, col);
  }
}

class GomokuArbiter {
  constructor() {
    LocalContractStorage.defineMapProperties(this, {
      games: {
        parse: str => (str ? Game.fromString(str) : null),
        stringify: game => (game ? game.toString() : 'null'),
      },
      players: {
        parse: str => (str ? Player.fromString(str) : null),
        stringify: player => (player ? player.toString() : null),
      },
    });
  }

  init() {
    this.players.set('abc', new Player('def', 'abc', 'zzz'));
  }

  getPlayer(address) {
    return this.players.get(address);
  }

  submitGame(id, players, moves) {
    let game = this.games.get(id);

    if (players.length !== 2) {
      throw new Error('Invalid number of players');
    }

    const playerAccs = players.map((p) => {
      const playerAcc = this.players.get(p.address);
      if (playerAcc) {
        return playerAcc;
      }
      const player = new Player(p.nickName, p.address, p.pubkey);
      this.players.set(player.address, player);
      return player;
    });

    if (!game) {
      game = new Game(id, players.map(player => player.address));
    }

    if (game.final) {
      return;
    }

    game.playerAccs = playerAccs;

    game.moves.forEach((move) => {
      game.applyMoveNoVerify(move);
    });

    moves.forEach((move) => {
      game.applyMove(move);
      const win = game.isWin();
      if (win) {
        game.final = true;
        game.winner = (win === 'X') ? game.players[0] : game.players[1];
      } else if (game.isDraw()) {
        game.final = true;
        game.winner = null;
      }
    });
    this.games.set(game.id, game);

    if (game.final) {
      let Sa = 0.5;
      if (game.winner === game.players[0]) {
        Sa = 1;
        game.playerAccs[0].wins += 1;
      } else if (game.winner) {
        Sa = 0;
      } else {
        game.playerAccs[0].draws += 1;
      }

      let Sb = 0.5;
      if (game.winner === game.players[1]) {
        Sb = 1;
        game.playerAccs[1].wins += 1;
      } else if (game.winner) {
        Sb = 0;
      } else {
        game.playerAccs[1].draws += 1;
      }

      const Ra = game.playerAccs[0].elo;
      const Rb = game.playerAccs[1].elo;

      const Ea = 1 / (1 + Math.pow(10, ((Rb - Ra) / 400)));
      const Eb = 1 / (1 + Math.pow(10, ((Ra - Rb) / 400)));

      game.playerAccs[0].elo = Ra + (16 * (Sa - Ea));
      game.playerAccs[1].elo = Rb + (16 * (Sb - Eb));

      game.playerAccs[0].games += 1;
      game.playerAccs[1].games += 1;
    }

    this.players.set(game.playerAccs[0].address, game.playerAccs[0]);
    this.players.set(game.playerAccs[1].address, game.playerAccs[1]);
  }
}

module.exports = GomokuArbiter;
