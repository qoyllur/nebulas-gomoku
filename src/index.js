import { HttpRequest, Neb } from 'nebulas';
import { idgen } from './idgen.js';
import { Account } from './account.js';
import { Messaging } from './messaging.js';
import { Game, BOARD_ROWS, BOARD_COLS } from './game.js';
import { setLoading, removeLoading } from './splash.js';
import './style.css';
import './loading.gif';

require('file-loader?name=[name].[ext]!./index.html');

const room = null;

/* global localStorage, document, location */

const dapp = 'n1y5oan4ggUurg7S5DVffNB1vneb6uHro6H';

const account = Account.fromLocalStorage('nebulas-gomoku');

const hasNebPay = (typeof (webExtensionWallet) !== 'undefined');

let messaging = null;
let game = null;
let myMark = null;
let handshakeDone = false;
let players = [];

const sayHi = (id) => {
  const msg = [id, 'hi', account.nickName, account.pub];
  const sig = account.sign(JSON.stringify(msg));
  msg.push(sig);
  messaging.send(msg);
};

const sendMove = (row, col) => {
  const move = game.signMove(row, col);
  messaging.send(move);
};

const handleHi = (id, msg) => {
  const [id_msg, hi, nickName, pub, sig] = msg;

  if (id !== id_msg) {
    return false;
  }

  if (hi !== 'hi') {
    return false;
  }

  let player = Account.fromPublicKeyNickName(pub, nickName);

  if (player.address === account.address) {
    player = account;
  }

  if (!player.verify(JSON.stringify([id, hi, nickName, pub]), sig)) {
    return false;
  }

  if (players.length > 1) {
    return false;
  }

  for (let idx = 0; idx < players.length; idx++) {
    if (players[idx].address === player.address) {
      return false;
    }
  }

  players.push(player);

  if (players.length < 2) {
    return false;
  }

  const opponent = (players[0].address == account.address) ?
    players[1] : players[0];

  for (let i = 0; i < 8; i++) {
    const myNum = (account.address.charCodeAt(i) + id.charCodeAt(i) - 96) % 74;
    const opNum = (opponent.address.charCodeAt(i) + id.charCodeAt(i) - 96) % 74;

    if (myNum > opNum) {
      myMark = 'X';
      players = [account, opponent];
    } else if (myNum < opNum) {
      myMark = 'O';
      players = [opponent, account];
    }
  }

  game = new Game(id, players, myMark);

  handshakeDone = true;

  console.log('handshake done!');
  console.log(players);

  game.updateBoard();
};

const handleMove = (id, msg) => {
  const [id_msg, tag] = msg;

  if (id !== id_msg) {
    return false;
  }

  if (tag !== 'move') {
    return false;
  }

  if (game.verifyMove(msg)) {
    game.applyMove(msg);
    game.updateBoard();
  }
};

const squareSelected = (evt) => {
  const square = evt.target;
  if (!handshakeDone) {
    return;
  }
  if (game.winner || game.draw) {
    return;
  }
  if (square.className.match(/marker/)) {
    alert('Sorry, that space is taken!  Please choose another square.');
  } else {
    const n = parseInt(square.getAttribute('id'), 10);
    const [row, col] = [Math.floor(n / BOARD_COLS), n % BOARD_COLS];
    sendMove(row, col);
  }
};

const NebPay = require('nebpay');

const nebPay = new NebPay();

const invokeMethod = (to, value, method, args, callback) => {
  nebPay.call(
    to, value, method, JSON.stringify(args),
    {
      listener: (resp) => {
        if (callback) {
          callback(resp);
        }
      },
    },
  );
};

const simulateMethod = (to, value, method, args, callback) => {
  nebPay.simulateCall(
    to, value, method, JSON.stringify(args),
    {
      listener: (resp) => {
        if (callback) {
          callback(resp);
        }
      },
      gasLimit: 1000000,
    },
  );
};

const init = () => {
  console.log('gomoku');

  console.log(account);

  if (!account.nickName) {
    const updateNickName = () => {
      const nickName = document.getElementById('nickname').value;
      console.log(nickName);
      if (nickname) {
        account.nickName = nickName;
        account.save('nebulas-gomoku');
        location.reload();
      }
    };

    document.getElementById('submitnickname').addEventListener('click', () => {
      updateNickName();
    });

    document.getElementById('nickname').addEventListener('keypress', (e) => {
      const keyCode = e.which;
      if (keyCode === 13) {
        updateNickName();
      }
    });

    return;
  }

  document.getElementById('loginscreen').classList.add('hidden');

  const table = document.getElementById('board');
  let n = 0;
  for (let col = 0; col < BOARD_COLS; col += 1) {
    const tr = document.createElement('tr');
    for (let row = 0; row < BOARD_ROWS; row += 1) {
      const td = document.createElement('td');
      td.setAttribute('id', n);
      tr.appendChild(td);
      n += 1;
    }
    table.appendChild(tr);
  }

  document.getElementById('newgame').onclick = () => {
    location.href = location.href.replace(location.hash, '');
  };

  let down = 'mousedown';
  if ('createTouch' in document) { down = 'touchstart'; }
  const squares = document.getElementsByTagName('td');
  for (let s = 0; s < squares.length; s++) {
    squares[s].addEventListener('mousedown', evt => squareSelected(evt), false);
  }

  if (!location.hash) {
    location.hash = idgen(8);
  }

  const roomId = location.hash.replace('#', '');

  document.getElementById('gameres').innerHTML =
      '^^ Send this URL to your friend, then wait here for them to show up';
  document.getElementById('gameres').setAttribute('style', 'color: black')

  messaging = new Messaging(roomId, (msg) => {
    console.log(msg);
    handleHi(roomId, msg);
    if (handshakeDone) {
      handleMove(roomId, msg);
    }
  });

  sayHi(roomId);

  document.getElementById('blockchain').addEventListener('click', () => {
    if (!game) {
      return;
    }
    if (!hasNebPay) {
      return;
    }
    invokeMethod(dapp, 0, 'submitGame', game.getGameText(), () => {
    });
  });

  setInterval(() => {
    if (!hasNebPay) {
      return;
    }
    simulateMethod(dapp, 0, 'getPlayer', [account.address], (resp) => {
      if (resp.result) {
        const result = JSON.parse(resp.result);
        if (result.elo) {
          const div = document.getElementById('elo');
          div.innerHTML = `Your ELO rating is ${Math.floor(result.elo)}`;
        }
      }
      console.log(resp);
    });
  }, 10000);
};

window.init = init;
