# Gomoku game on Nebulas blockchain

This is distributed per-to-peer battlefield on Nebulas blockchain.

How to play:

1. Share game URL with your friend to start playing.
2. Each move is signed with player's own private key which makes it impossible
   to cheat.
3. Submit the game to the smart contract on the Nebulas blockchain. Smart
   contract validates the game and updates player's ratings.
