const ec = require('elliptic');
const md5 = require('js-md5');

const curve = new ec.ec('p192');

/* global localStorage */

/* eslint no-bitwise: ["error", { "allow": ["&"] }] */

class Account {
  static fromLocalStorage(lsKey) {
    const accString = localStorage.getItem(lsKey);
    let ecKey = null;
    let nickName = null;

    if (accString) {
      const accEncoded = JSON.parse(accString);
      ecKey = curve.keyFromPrivate(accEncoded.priv);
      ({ nickName } = accEncoded);
    } else {
      ecKey = curve.genKeyPair();
      const accEncoded = {
        pub: ecKey.getPublic().encode('hex'),
        priv: ecKey.getPrivate('hex'),
        nickName: null,
      };
      localStorage.setItem(
        lsKey,
        JSON.stringify(accEncoded),
      );
    }

    return new Account(ecKey, false, nickName);
  }

  static fromPublicKeyNickName(pubKey, nickName) {
    const ecKey = curve.keyFromPublic(pubKey, 'hex');
    return new Account(ecKey, true, nickName);
  }

  save(lsKey) {
    const accEncoded = {
      pub: this.pub,
      priv: this.priv,
      nickName: this.nickName,
    };
    localStorage.setItem(
      lsKey,
      JSON.stringify(accEncoded),
    );
  }

  constructor(ecKey, publicOnly, nickName) {
    this.address = md5(ecKey.getPublic().encode('hex'));
    this.pub = ecKey.getPublic().encode('hex');
    this.priv = publicOnly ? null : ecKey.getPrivate('hex');
    this.key = ecKey;
    this.nickName = nickName;
  }

  getNickName() {
    return this.nickName;
  }

  getPub() {
    return this.pub;
  }

  getPriv() {
    return this.priv;
  }

  getKey() {
    return this.key;
  }

  getAddress() {
    return this.address;
  }

  sign(msg) {
    const sig = this.key.sign(md5(msg));
    return Array.from(
      sig.toDER(),
      byte => (`0${(byte & 0xFF).toString(16)}`).slice(-2),
    ).join('');
  }

  verify(msg, signature) {
    return this.key.verify(md5(msg), signature);
  }
}

module.exports = {
  Account,
};
