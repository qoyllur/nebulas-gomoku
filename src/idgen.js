const ALPHABET = '0123456789'
                  + 'abcdefghijklmnopqrstuvwxyz'
                  + 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';

const idgen = (len) => {
  let rtn = '';
  for (let i = 0; i < len; i += 1) {
    rtn += ALPHABET.charAt(Math.floor(Math.random() * ALPHABET.length));
  }
  return rtn;
};

module.exports = {
  idgen,
};
