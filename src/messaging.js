
const firebase = require('@firebase/app').firebase;
require('@firebase/database');

// Initialize Firebase
var config = {
  apiKey: "AIzaSyAnva7JhxIEpuao8upSNgKDRveAr2t8nbA",
  authDomain: "nebulas-gomoku.firebaseapp.com",
  databaseURL: "https://nebulas-gomoku.firebaseio.com",
  projectId: "nebulas-gomoku",
  storageBucket: "nebulas-gomoku.appspot.com",
  messagingSenderId: "423502355938"
};

firebase.initializeApp(config);

const database = firebase.database();

class Messaging {
  constructor(roomId, onMessage) {
    this.room = database.ref().child('rooms/' + roomId);
    this.room.child('messages').on('child_added', (snapshot) => {
      const msg = JSON.parse(snapshot.val().message);
      if (onMessage) {
        onMessage(msg);
      }
    });
  }

  send(msg) {
    const data = {
      message: JSON.stringify(msg)
    };
    const key = this.room.push().key;
    this.room.child('messages/' + key).set(data);
  }
}

module.exports = {
  Messaging,
};
