
/* global document */

const setLoading = () => {
  document.querySelector('body').classList.add('loading');
};

const removeLoading = () => {
  document.querySelector('body').classList.remove('loading');
};

module.exports = {
  setLoading,
  removeLoading,
};
