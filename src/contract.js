const nebulas = require('nebulas');
const splash = require('./splash.js');

/* global alert */

class SmartContract {
  constructor(neb, chainId, address) {
    this.neb = neb;
    this.chainId = chainId;
    this.address = address;
  }

  waitForTx(txhash, callback) {
    const check = () => {
      this.neb.api.getTransactionReceipt(txhash)
        .then((resp) => {
          // console.log(resp);
          if (resp.status !== 1 && resp.status !== 0) {
            setTimeout(() => check(), 2000);
          } else {
            // console.log(resp.status === 1 ? 'success' : 'fail');
            splash.removeLoading();
            if (callback) {
              callback(resp.status, JSON.parse(resp.execute_result));
            }
          }
        });
    };
    setTimeout(() => check(), 2000);
  }

  check(account, fun, args, callback, showSplash = true) {
    const params = {};

    if (showSplash) {
      splash.setLoading();
    }

    params.from = account.getAddressString();
    params.to = this.address;
    params.gasLimit = 200000;
    params.gasPrice = 1000000;
    params.value = 0;
    params.contract = { function: fun, args: JSON.stringify(args) };

    this.neb.api.getAccountState(params.from).then((accStateResp) => {
      params.nonce = parseInt(accStateResp.nonce, 10) + 1;

      this.neb.api.call({
        from: params.from,
        to: params.to,
        value: params.value,
        nonce: params.nonce,
        gasPrice: params.gasPrice,
        gasLimit: params.gasLimit,
        contract: params.contract,
      }).then((callResp) => {
        if (showSplash) {
          splash.removeLoading();
        }
        callback(JSON.parse(callResp.result));
      }).catch((err) => {
        console.log(err);
        if (showSplash) {
          splash.removeLoading();
        }
        alert(err);
      });
    }).catch((err) => {
      console.log(err);
      if (showSplash) {
        splash.removeLoading();
      }
      alert(err);
    });
  }

  call(account, fun, args, callback) {
    const params = {};

    splash.setLoading();

    params.from = account.getAddressString();
    params.to = this.address;
    params.gasLimit = 200000;
    params.gasPrice = 1000000;
    params.value = 0;
    params.contract = { function: fun, args: JSON.stringify(args) };

    this.neb.api.getAccountState(params.from).then((accStateResp) => {
      params.nonce = parseInt(accStateResp.nonce, 10) + 1;

      this.neb.api.call({
        from: params.from,
        to: params.to,
        value: params.value,
        nonce: params.nonce,
        gasPrice: params.gasPrice,
        gasLimit: params.gasLimit,
        contract: params.contract,
      }).then((callResp) => {
        const tx = new nebulas.Transaction(
          this.chainId, account,
          params.to, params.value, params.nonce,
          params.gasPrice, params.gasLimit,
          params.contract,
        );

        tx.signTransaction();

        this.neb.api.sendRawTransaction(tx.toProtoString())
          .then((sendResp) => {
            this.waitForTx(sendResp.txhash, (status, result) => { callback(result); });
          })
          .catch((err) => {
            console.log(err);
            splash.removeLoading();
            alert(err);
          });
      }).catch((err) => {
        console.log(err);
        splash.removeLoading();
        alert(err);
      });
    }).catch((err) => {
      console.log(err);
      splash.removeLoading();
      alert(err);
    });
  }
}

module.exports = {
  SmartContract,
};
