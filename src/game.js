
/* global document */

const BOARD_ROWS = 15;
const BOARD_COLS = 15;

class Game {
  constructor(id, players, myMark) {
    this.id = id;
    this.currentMove = 0;
    this.players = players;
    this.board = new Array((BOARD_ROWS * BOARD_COLS) + 1).join(' ');
    this.myMark = myMark;
    this.lastSig = '';
    this.moves = [];
    this.winner = null;
    this.draw = false;
  }

  isWin() {
    const rDiagonal = (r, c) => {
      let w = 0;
      const sq = this.board[(r * BOARD_COLS) + c];
      for (let x = c, y = r; x < BOARD_COLS && y < BOARD_ROWS; x += 1, y += 1) {
        if (sq === this.board[(y * BOARD_COLS) + x]) {
          w += 1;
        } else {
          break;
        }
      }
      if (w >= 5) {
        console.log('we have a winner');
      }
      return (w >= 5);
    };

    const lDiagonal = (r, c) => {
      let w = 0;
      const sq = this.board[(r * BOARD_COLS) + c];
      for (let x = c, y = r; x >= 0 && y < BOARD_ROWS; x -= 1, y += 1) {
        if (sq === this.board[(y * BOARD_COLS) + x]) {
          w += 1;
        } else {
          break;
        }
      }
      if (w >= 5) {
        console.log('we have a winner');
      }
      return (w >= 5);
    };

    const rightward = (r, c) => {
      let w = 0;
      const sq = this.board[(r * BOARD_COLS) + c];
      for (let x = c; x < BOARD_COLS; x += 1) {
        if (sq === this.board[(r * BOARD_COLS) + x]) {
          w += 1;
        } else {
          break;
        }
      }
      if (w >= 5) {
        console.log('we have a winner', r, c);
      }
      return (w >= 5);
    };

    const downward = (r, c) => {
      let w = 0;
      const sq = this.board[(r * BOARD_COLS) + c];
      for (let y = r; y < BOARD_ROWS; y += 1) {
        if (sq === this.board[(y * BOARD_COLS) + c]) {
          w += 1;
        } else {
          break;
        }
      }
      if (w >= 5) {
        console.log('we have a winner');
      }
      return (w >= 5);
    };

    for (let i = 0; i < BOARD_ROWS; i += 1) {
      for (let j = 0; j < BOARD_COLS; j += 1) {
        if (this.board[(i * BOARD_COLS) + j] !== ' ') {
          if (downward(i, j) ||
              rightward(i, j) ||
              lDiagonal(i, j) ||
              rDiagonal(i, j)) {
            return this.board[(i * BOARD_COLS) + j];
          }
        }
      }
    }

    return false;
  }

  isDraw() {
    for (let i = 0; i < BOARD_ROWS * BOARD_COLS; i += 1) {
      if (this.board[i] === ' ') {
        return false;
      }
    }
    return true;
  }

  updateBoard() {
    const playerX = document.getElementById('X');
    const playerO = document.getElementById('O');

    if (this.myMark === 'X') {
      playerX.textContent = 'You (X)';
      playerO.textContent = 'Opponent (O)';
    } else {
      playerO.textContent = 'You (O)';
      playerX.textContent = 'Opponent (X)';
    }

    if (this.currentMove % 2 === 0) {
      playerX.className = 'current-player';
      playerO.className = '';
    } else {
      playerO.className = 'current-player';
      playerX.className = '';
    }

    for (let i = 0; i < BOARD_COLS * BOARD_ROWS; i += 1) {
      const square = document.getElementById(i);
      const marker = this.board[i];
      if (marker !== ' ' && square.children.length === 0) {
        const m = document.createElement('div');
        m.className = `${marker}-marker`;
        square.appendChild(m);
      }
    }

    const win = this.isWin();
    if (win) {
      this.winner = (win === 'X') ? this.players[0] : this.players[1];
    } else if (this.isDraw()) {
      this.draw = true;
    }

    if (win === this.myMark) {
      document.getElementById('gameres').innerHTML = 'You won!';
      document.getElementById('gameres').setAttribute('style', 'color: red');
      playerX.className = '';
      playerO.className = '';
    } else if (win) {
      document.getElementById('gameres').innerHTML = 'You lost!';
      document.getElementById('gameres').setAttribute('style', 'color: blue');
      playerX.className = '';
      playerO.className = '';
    } else if (this.draw) {
      document.getElementById('gameres').innerHTML = 'It is a draw!';
      document.getElementById('gameres').setAttribute('style', 'color: black');
      playerX.className = '';
      playerO.className = '';
    } else {
      document.getElementById('gameres').setAttribute('style', 'color: white');
    }
  }

  isValidMove(row, col) {
    if (this.board[(row * BOARD_COLS) + col] !== ' ') {
      return false;
    }
    return true;
  }

  applyMove(move) {
    if (!this.verifyMove(move)) {
      return false;
    }

    const [, , address, , row, col, sig] = move;
    const mark = (this.players[0].address === address) ? 'X' : 'O';
    const index = (row * BOARD_COLS) + col;

    this.board = this.board.substr(0, index) + mark
                 + this.board.substr(index + mark.length);
    this.currentMove += 1;
    this.lastSig = sig;
    this.moves.push(move);

    return true;
  }

  signMove(row, col) {
    const player = this.players[this.currentMove % 2];
    const moveString = JSON.stringify([this.id, 'move', player.address,
      this.currentMove, row, col, this.lastSig]);
    const sig = player.sign(moveString);
    return [this.id, 'move', player.address, this.currentMove, row, col, sig];
  }

  verifyMove(move) {
    const player = this.players[this.currentMove % 2];

    const [id, tag, address, moveN, row, col, sig] = move;

    if (id !== this.id) {
      return false;
    }

    if (tag !== 'move') {
      return false;
    }

    if (address !== player.address) {
      return false;
    }

    if (moveN !== this.currentMove) {
      return false;
    }

    const moveString = JSON.stringify([id, 'move', address,
      moveN, row, col, this.lastSig]);

    if (!player.verify(moveString, sig)) {
      return false;
    }

    return this.isValidMove(row, col);
  }

  getGameText() {
    return [
      this.id,
      this.players.map(p => ({
        address: p.address,
        nickName: p.nickName,
        pubkey: p.pub,
      })),
      this.moves,
    ];
  }
}

module.exports = {
  Game,
  BOARD_ROWS,
  BOARD_COLS,
};
